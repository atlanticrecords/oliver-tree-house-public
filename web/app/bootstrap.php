<?php
require __DIR__ . '/vendor/autoload.php';
session_start();

$settings = require __DIR__ . '/src/settings.php';

$container = new \League\Container\Container;
$container->delegate(new \Slim\Container($settings));

$container->delegate(
  new \League\Container\ReflectionContainer
);

$container->add('upload_directory', __DIR__ . '/../public/uploads');

$app = new \Slim\App($container);

require __DIR__ . '/src/dependencies.php';
require __DIR__ . '/src/middleware.php';
require __DIR__ . '/src/routes.php';
