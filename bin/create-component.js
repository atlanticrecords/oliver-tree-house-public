#!/usr/bin/env node
const path = require('path');
const fs = require('fs');
const args = process.argv.slice(2);

const toCamelCase = str => str.replace(/-./g, c => c.substring(1).toUpperCase());
const toPascalCase = str => str.replace(/\w\S*/g, m => m.charAt(0).toUpperCase() + m.substr(1));
const projectRoot = process.cwd();
const componentName = args[0];
const componentNameCamelCase = toCamelCase(componentName);
const componentNamePascalCase = toPascalCase(componentNameCamelCase);
const componentDir = path.resolve(projectRoot, `./src/javascripts/components/${componentName}`);
fs.mkdirSync(componentDir);

const jsxTemplate = componentName => `import React from 'react';

const ${componentName} = () => {};

export default ${componentName};
`;

const indexTemplate = componentName => `import { connect } from 'react-redux';
import { compose } from 'redux';
import ${toPascalCase(toCamelCase(componentName))} from './${componentName}';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(${toPascalCase(toCamelCase(componentName))});
`;

const reducerTemplate = componentName => `import actions from './actions';

const initialState = {
};

const ${componentName} = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    default:
      return state;
  }
};

export default ${componentName};
`;

const actionsTemplate = () => `const actions = {};

export default actions;
`;

const files = [
  {
    name: 'index.js',
    template: indexTemplate(componentName),
  },
  {
    name: `${componentName}.jsx`,
    template: jsxTemplate(componentNamePascalCase),
  },
  {
    name: 'reducer.js',
    template: reducerTemplate(componentNameCamelCase),
  },
  {
    name: 'actions.js',
    template: actionsTemplate(),
  }
];

files.forEach(file => {
  fs.writeFileSync(`${componentDir}/${file.name}`, file.template);
});
