#!/usr/bin/env node
const xlsx = require('node-xlsx');
const fs = require('fs');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const args = process.argv.slice(2);

const excelPath = args[0];
const dataPath = args[1];

try {
  const obj = xlsx.parse(excelPath);

  const pageCodeSheet = obj.filter(s => s.name === 'Page Code')[0];

  // Concat all filled cells into one string
  const pageCode = pageCodeSheet['data']
        .map(row => row.slice(0, 4).filter(c => c.length > 0))
        .filter(row => row.length > 0)
        .reduce((acc, curr) => acc.concat(curr)).join('');

  // Load it into a DOM
  const { window } = new JSDOM(pageCode);

  // Find first script tag in DOM, get digitalData object as string
  const str = window.document.getElementsByTagName('script')[0].innerHTML.split('=')[1];

  // Eval the string into an object
  const digitalData = eval(`(${str})`);

  // Write to data.json or dump to console
  if (dataPath) {
    const siteData = JSON.parse( fs.readFileSync(dataPath) );
    siteData.omniture = digitalData;
    fs.writeFileSync(dataPath, JSON.stringify(siteData, null, 2));
    console.log(`${dataPath} written`);
  } else {
    console.log(JSON.stringify( digitalData ));
  }
} catch (err) {
  console.log(err);
}
