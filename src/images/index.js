import bedroom from './rooms/olivertree_bedroom-01.png';
import artroom from './rooms/olivertree_artroom-01.png';
import attic from './rooms/olivertree_attic.png';
import closet from './rooms/olivertree_closet-01.png';
import gameroom from './rooms/olivertree_gameroom-01.png';
import garage from './rooms/garage.png';
import garageDoor from './rooms/garagedoor_extended.png';
import studio from './rooms/olivertree_recordingstudio-01.png';
import theater from './rooms/olivertree_theater-01.png';

import apple from './icons/apple.svg';
import appleHover from './icons/apple_hover.svg';
import facebook from './icons/facebook.svg';
import facebookHover from './icons/facebook_hover.svg';
import reddit from './icons/reddit.svg';
import redditHover from './icons/reddit_hover.svg';
import twitter from './icons/twitter.svg';
import twitterHover from './icons/twitter_hover.svg';
import tiktok from './icons/tiktok.svg';
import tiktokHover from './icons/tiktok_hover.svg';
import youtube from './icons/youtube.svg';
import youtubeHover from './icons/youtube_hover.svg';
import wikipedia from './icons/wikipedia.svg';
import wikipediaHover from './icons/wikipedia_hover.svg';
import instagram from './icons/instagram.svg';
import instagramHover from './icons/instagram_hover.svg';
import soundcloud from './icons/soundcloud.svg';
import soundcloudHover from './icons/soundcloud_hover.svg';
import spotify from './icons/spotify.svg';
import spotifyHover from './icons/spotify_hover.svg';

export linkIcon from './icons/link.svg';
export linkIconHover from './icons/link_hover.svg';

const icons = {
  instagram: {
    default: instagram,
    hover: instagramHover,
    href: 'https://www.instagram.com/OliverTree',
    linkName: 'Instagram',
  },
  tiktok: {
    default: tiktok,
    hover: tiktokHover,
    href: 'http://tiktok.com/@olivertree',
    linkName: 'TikTok',
  },
  youtube: {
    default: youtube,
    hover: youtubeHover,
    href: 'https://www.youtube.com/channel/UCHcb3FQivl6xCRcHC2zjdkQ',
    linkName: 'Youtube',
  },
  facebook: {
    default: facebook,
    hover: facebookHover,
    href: 'https://www.facebook.com/olivertreemusic',
    linkName: 'Facebook',
  },
  twitter: {
    default: twitter,
    hover: twitterHover,
    href: 'https://twitter.com/olivertree',
    linkName: 'Twitter',
  },
};

const icons2 = {
  vk: {
    default: wikipedia,
    hover: wikipediaHover,
    href: 'https://vk.com/olivertree',
    linkName: 'VK',
  },
  soundcloud: {
    default: soundcloud,
    hover: soundcloudHover,
    href: 'https://soundcloud.com/olivertree',
    linkName: 'Soundcloud',
  },
  spotify: {
    default: spotify,
    hover: spotifyHover,
    href: 'https://open.spotify.com/artist/6TLwD7HPWuiOzvXEa3oCNe',
    linkName: 'Spotify',
  },
  apple: {
    default: apple,
    hover: appleHover,
    href: 'https://smarturl.it/OliverTreeAM',
    linkName: 'Apple Music',
  },
  reddit: {
    default: reddit,
    hover: redditHover,
    href: 'https://www.reddit.com/r/olivertree/',
    linkName: 'Reddit',
  },
};

const rooms = {
  bedroom,
  artroom,
  attic,
  closet,
  garage,
  garageDoor,
  gameroom,
  studio,
  theater,
};

export bg from './bg-2.svg';
export fg from './fg-3.png';
export clouds from './clouds.svg';
export stars from './stars.svg';
export logo from './logo.svg';
export ufo from './ufo.svg';

export { rooms, icons, icons2 };
