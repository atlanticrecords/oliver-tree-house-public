import anime from 'animejs/lib/anime.es.js';

const modalProps = {
  opacity: 0,
  easing: 'linear',
};

const toggleModal = (page, modal, show, speed = 300) => {
  const targetIn = show ? modal : page;
  const targetOut = !show ? modal : page;

  return anime({
    targets: targetOut,
    ...modalProps,
    duration: speed,
    complete: () => {
      targetOut.style.display = 'none';
      targetIn.style.display = 'flex';
      //targetIn.style.visibility = 'visible';
      anime({
        targets: targetIn,
        duration: speed,
        easing: 'easeInOutSine',
        opacity: 1,
      });
    }
  });
};

const reveal = item => {
  anime({
    targets: item,
    duration: 400,
    easing: 'linear',
    opacity: 1,
    begin: () => {
      //item.style.visibility = 'visible';
    }
  });
};

export { toggleModal, reveal };
