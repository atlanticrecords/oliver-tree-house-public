import React from 'react';

const Socials = () => (
  <div className="socials">
    {Object.entries(window.atlConfig.socials).map(([key, val]) => (
      <a href={val} key={key} target="_blank" rel="noopener noreferrer" className="mx-2 mx-md-3">
        <i className={`atl-icon atl-icon--${key}`} />
      </a>
    ))}
  </div>
);

export default Socials;
