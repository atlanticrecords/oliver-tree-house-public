import { connect } from 'react-redux';

import SpotifyAuthBtn from './topsify-auth-btn';
import { fetchTopsifyLogin } from './actions';

const mapStateToProps = state => ({
  isAuthorized: state.topsify.authorized,
});

const mapDispatchToProps = dispatch => ({
  fetchTopsifyLogin: payload => dispatch(fetchTopsifyLogin(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SpotifyAuthBtn);
