import { connect } from 'react-redux';
import { compose } from 'redux';
import { showModal } from 'components/modal/actions';
import HelloWorld from './hello-world';

const mapStateToProps = state => ({
  showModal: state.ui.modal.show,
});

const mapDispatchToProps = dispatch => ({
  showModal: payload => dispatch(showModal(payload)),
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(HelloWorld);
