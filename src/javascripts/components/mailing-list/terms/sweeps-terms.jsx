import React from 'react';

const SweepsTerms = () => (
  <div className="terms terms--sweeps-terms">
    No purchase or payment necessary to enter or win. A purchase will not increase your chances of winning. Sweepstakes only open to legal residents of the 50 united states and the district of columbia who are at least 18 years old at the time of entry. Void where prohibited.
  </div>
);

export default SweepsTerms;
