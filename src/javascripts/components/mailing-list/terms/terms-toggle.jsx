import React from 'react';

class TermsToggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      opened: false,
    };
  }

  toggleTermsBody = () => {
    this.setState({ opened: !this.state.opened });
  };

  render() {
    return (
      <div className="terms">
        <button
          onClick={this.toggleTermsBody}
          className="btn btn-link"
        >
          <span className="small micro">{this.state.opened ? 'Hide' : 'Terms'}</span>
        </button>
        <div
          className="terms__body micro"
          style={{
            display: this.state.opened ? 'block' : 'none',
          }}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default TermsToggle;
