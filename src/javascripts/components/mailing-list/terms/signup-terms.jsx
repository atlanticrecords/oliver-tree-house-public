import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { PAGE_CONFIG } from 'utils/config';

const SignupTerms = ({ artist, privacy }) => (
  <Fragment>
    By submitting my information, I agree to receive personalized updates and marketing messages
    about {artist} based on my information, interests, activities, website visits and device data and
    in accordance with the&nbsp;
    <a
      href={privacy}
      target="_blank"
      rel="noopener noreferrer"
    >
      privacy policy
    </a>. I understand that I can opt-out at any time by emailing&nbsp;
    <a href="mailto:privacypolicy@wmg.com">privacypolicy@wmg.com</a>.
  </Fragment>
);

SignupTerms.defaultProps = {
  artist: PAGE_CONFIG.artist,
  privacy: PAGE_CONFIG.privacy,
};

SignupTerms.propTypes = {
  artist: PropTypes.string.isRequired,
};

export default SignupTerms;
