import React from 'react';
import './checkbox.scss';

const Checkbox = () => {
  return (
    <div className="mlist__checkbox mr-3">
      <label htmlFor="termsagree">
        <input className="checkbox" id="termsagree" type="checkbox" name="termsagree" required />
        <i></i>
      </label>
    </div>
  );
};

export default Checkbox;
