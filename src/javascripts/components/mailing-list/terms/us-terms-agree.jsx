import React from 'react';
import ContestTerms from './contest-terms';
import Checkbox from './checkbox';

const USTermsAgree = ({ rules }) => (
  <div id="mlist-terms-us" className="d-flex my-3">
    <Checkbox />
    <ContestTerms rules={rules} />
  </div>
);

export default USTermsAgree;
