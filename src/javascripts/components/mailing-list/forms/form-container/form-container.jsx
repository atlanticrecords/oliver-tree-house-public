import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { scrapeEmail } from 'utils/mailing-list';
import { trackSignup } from 'utils/tracking';

const FormContainer = ({ onSuccess, children }) => {
  const formEl = useRef(null);

  const handleSubmit = async event => {
    event.preventDefault();

    const formData = Object.fromEntries(new FormData(formEl.current));
    const response = await scrapeEmail(formData);
    console.log(response);

    if (response.status === 'success') {
      onSuccess(formData, response);
      trackSignup();
    }
  };

  return (
    <form
      className="mlist__signup-form mlist--single-field-form"
      id="mlist-primary-form"
      onSubmit={handleSubmit}
      ref={formEl}
    >
      {children}
    </form>
  );
};

FormContainer.defaultProps = {
  onSuccess: () => true,
};

FormContainer.propTypes = {
  onSuccess: PropTypes.func,
};

export default FormContainer;
