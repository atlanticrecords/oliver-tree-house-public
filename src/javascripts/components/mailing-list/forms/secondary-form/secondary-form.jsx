import React, { Fragment } from 'react';
import { SignupTerms, TermsToggle } from 'components/mailing-list/terms';
import { FirstNameField, PostalCodeField, CountryField, BirthdateField, SubmitBtn } from 'components/mailing-list/fields';

const SecondaryForm = ({secondaryNewsletterId, artist, privacyUrl}) => (
  <form
    className="mlist__signup-form"
    id="mlist-secondary-form"
    action="https://signup.wmg.com/register?geoip=true"
    method="GET"
    ref={this.formEl}
  >
    <FirstNameField />
    <PostalCodeField />
    <CountryField />
    <BirthdateField />
    <div className="mlist-field hidden mlist-secondary-values" style={{ display: 'none' }}>
      <input id="newsletterId" type="hidden" name="newsletterId" value={secondaryNewsletterId} />
    </div>
    <div className="mlist-field mlist-field--checks">
      <input
        type="checkbox"
        id="global-list"
        className="global-list-checkbox"
        name="newsletterId"
        value="50"
      />
      <label className="global-list--label" htmlFor="global-list">
        Sign me up to discover more artists list {artist} and other offers
      </label>
    </div>
    <SubmitBtn />
    <TermsToggle>
      <SignupTerms artist={artist} privacyUrl={privacyUrl} />
    </TermsToggle>
  </form>
);

export default SecondaryForm;
