import { connect } from 'react-redux';
import SecondaryForm from './secondary-form';

const mapStateToProps = state => ({
  secondaryNewsletterId: window.mlistConfig.secondaryId,
  artist: window.pageConfig.artist,
  privacyUrl: window.pageConfig.privacy,
  pronoun: window.pageConfig.pronoun,
});

export default connect(mapStateToProps)(SecondaryForm);
