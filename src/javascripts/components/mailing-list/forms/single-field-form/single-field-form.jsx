import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { SignupTerms, TermsToggle } from 'components/mailing-list/terms';
import { EmailField, PrimaryValues, SubmitBtn } from 'components/mailing-list/fields';
import './single-field-form.scss';

const EmailForm = () => (
  <Fragment>
    <EmailField />
    <PrimaryValues />
    <SubmitBtn btnText="Subscribe" />
    <TermsToggle>
      <SignupTerms />
    </TermsToggle>
  </Fragment>
);

export default EmailForm;
