import React, { useRef } from 'react';

const EmailField = () => {
  return (
    <div className="mlist-field mlist-field--email">
      <label htmlFor="email">Email Address</label>
      <input id="email" type="email" name="email" placeholder="Email Address" required minLength="0" />
      <div className="messages" />
    </div>
  );
};

export default EmailField;
