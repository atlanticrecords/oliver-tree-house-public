import React from 'react';

import BaseGate from './';

const withBaseGate = Component => props =>
  props.isAuthorized ? <Component {...props} /> : <BaseGate />;

export default withBaseGate;
