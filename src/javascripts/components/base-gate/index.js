import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import BaseGate from './base-gate';

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(BaseGate);
