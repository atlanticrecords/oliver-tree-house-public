import React from 'react';
import './base-gate.scss';

const BaseGate = () => (
  <section className="section gate">
    <h1>Base Gate</h1>
  </section>
);

export default BaseGate;
