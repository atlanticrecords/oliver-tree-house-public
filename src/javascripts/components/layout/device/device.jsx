import React from 'react';
import PropTypes from 'prop-types';

class Device extends React.Component {
  componentDidMount() {
    const { touchStart, isTouchDevice } = this.props;

    // Reject if already detected
    if (isTouchDevice) {
      return;
    }

    // Attempt to sniff for event
    if ('ontouchstart' in window || navigator.msMaxTouchPoints > 0) {
      touchStart();
      return;
    }

    // Pick up even on touch start as final attempt
    let touchedDetected;
    window.addEventListener(
      'touchstart',
      (touchedDetected = () => {
        window.removeEventListener('touchstart', touchedDetected, false);
      })
    );
  }

  render = () => null;
}

Device.propTypes = {
  touchStart: PropTypes.func.isRequired,
  isTouchDevice: PropTypes.bool.isRequired,
};

export default Device;
