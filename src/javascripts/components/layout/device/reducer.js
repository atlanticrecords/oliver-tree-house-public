import deviceActions from './actions';

const initialState = {
  touch: false,
};

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case deviceActions.TOUCH_START:
      return { ...state, touch: true };
    default:
      return state;
  }
};

export default modalReducer;
