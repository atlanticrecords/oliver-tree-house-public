import React from 'react';
import PropTypes from 'prop-types';

const Icon = ({ name, viewBox }) => (
  <span className={`icon icon--${name}`}>
    <svg className="icon__svg" viewBox={viewBox}>
      <use xlinkHref={`/images/icons.svg#${name}`} />
    </svg>
  </span>
);

Icon.defaultProps = {
  viewBox: '0 0 1 1',
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  viewBox: PropTypes.string,
};

export default Icon;
