import React from 'react';

const Header = () => (
  <header className="primary-header my-5">
    <h1>Christina Perri</h1>
  </header>
);

export default Header;
