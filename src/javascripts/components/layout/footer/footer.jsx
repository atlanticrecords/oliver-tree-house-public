import React from 'react';
import PropTypes from 'prop-types';
import { PAGE_CONFIG } from 'utils/config';

const Footer = ({ children, copyright, privacy, terms, adChoices }) => (
  <footer className="primary-footer p-3">
    {children}
    <div className="legal">
      <span className="copyright">{copyright}</span>
      <div className="footer-links">
        <a
          className="footer-link"
          href={privacy}
          target="_blank"
          rel="noreferrer noopener"
        >
          Privacy
        </a>
        <span>|</span>
        <a
          className="footer-link"
          href={terms}
          target="_blank"
          rel="noreferrer noopener"
        >
          Terms of Use
        </a>
        <span>|</span>
        <a
          className="footer-link"
          href={adChoices}
          target="_blank"
          rel="noreferrer noopener"
        >
          Ad Choices
        </a>
      </div>
    </div>
  </footer>
);

Footer.defaultProps = {
  copyright: PAGE_CONFIG.copyright,
  privacy: PAGE_CONFIG.privacy,
  terms: PAGE_CONFIG.terms,
  adChoices: PAGE_CONFIG.adChoices,
};

Footer.propTypes = {
  copyright: PropTypes.string.isRequired,
  privacy: PropTypes.string.isRequired,
  terms: PropTypes.string.isRequired,
  adChoices: PropTypes.string.isRequired,
};

export default Footer;
