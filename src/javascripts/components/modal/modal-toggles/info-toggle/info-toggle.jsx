import React from 'react';
import PropTypes from 'prop-types';

import { trackClick } from 'utils/tracking';

const InfoToggle = ({ showInfo }) => (
  <button
    type="button"
    className="btn btn--rounded info-toggle"
    onClick={() => {
      showInfo();
      trackClick('Help Click');
    }}
    style={{ fontWeight: 'normal' }}
  >
    ?
  </button>
);

InfoToggle.propTypes = {
  showInfo: PropTypes.func.isRequired,
};

export default InfoToggle;
