import React from 'react';
import PropTypes from 'prop-types';

import { trackClick } from 'utils/tracking';
import Icon from 'components/layout/icon';

const InspirationToggle = ({ showInspiration }) => (
  <button
    type="button"
    className="btn btn--rounded inspiration-toggle"
    onClick={() => {
      showInspiration();
      trackClick('Inspiration Click');
    }}
  >
    <Icon name="grid" />
  </button>
);

InspirationToggle.propTypes = {
  showInspiration: PropTypes.func.isRequired,
};

export default InspirationToggle;
