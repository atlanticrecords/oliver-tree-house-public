import React from 'react';
import PropTypes from 'prop-types';

import { trackClick } from 'utils/tracking';
import Icon from 'components/layout/icon';

const FinishImageToggle = ({ showFinishImage, finishImage }) => (
  <button
    className="finish-image-toggle"
    onClick={() => {
      finishImage();
      showFinishImage();
      trackClick('Share Icon Click');
    }}
  >
    <Icon name="share-icon" />
  </button>
);

FinishImageToggle.propTypes = {
  showFinishImage: PropTypes.func.isRequired,
  finishImage: PropTypes.func.isRequired,
};

export default FinishImageToggle;
