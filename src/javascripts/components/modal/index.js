import { connect } from 'react-redux';

import modalTypes from 'constants/modal-types';
import Modal from './modal';
import { hideModal } from './actions';

const mapStateToProps = state => ({
  type: modalTypes[state.ui.modal.type],
  closeButton: state.ui.modal.type !== 'About',
});

const mapDispatchToProps = dispatch => ({
  hideModal: () => dispatch(hideModal()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
