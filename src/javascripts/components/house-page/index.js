import { connect } from 'react-redux';
import { compose } from 'redux';
import { showModal } from 'components/modal/actions';
import HousePage from './house-page';

const mapStateToProps = state => ({
});

export default compose(
  connect(
    mapStateToProps,
    { showModal }
  )
)(HousePage);
