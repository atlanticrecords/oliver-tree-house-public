import React, { Fragment, useState, useEffect } from 'react';
import { logo, ufo, clouds, stars, icons, icons2 } from 'images';
import Footer from 'components/layout/footer';
import FormContainer, { SingleFieldForm } from 'components/mailing-list';
import Rellax from 'rellax';
import Foreground from './fg-3';
import './house-page.scss';

const socialIcons = { ...icons, ...icons2 };

const HousePage = ({ showModal }) => {
  const [socialMenuActive, setSocialMenuActive] = useState(false);
  const [showForm, setShowForm] = useState(true);

  useEffect(() => {
    const rellax = new Rellax('.rellax');
  }, []);

  const handleLinkIcon = evt => {
    console.log(evt);
    setSocialMenuActive(true);
  };

  const handleCloseMenu = () => setSocialMenuActive(false);

  return (
    <Fragment>
      <div className="header">
        <div className="logo px-3">
          <img src={logo} />
        </div>
        <img src={ufo} className="ufo rellax" />
        <img src={stars} className="stars" />
        <img src={clouds} className="clouds rellax" />
      </div>
      <div className="house-page">
        <div className="fg-wrapper">
          <Foreground linkIconHandler={handleLinkIcon} showModal={showModal} />
        </div>
        <Footer>
          <div className="px-2">
            {showForm ? (
              <FormContainer onSuccess={() => setShowForm(false)}>
                <SingleFieldForm />
              </FormContainer>
            ): (
              <div><h3>Thank You!</h3></div>
            ) }
          </div>
        </Footer>
        <button className="desktop-signup-btn" />
        <div className="desktop-signup">
          <div className="arrow_box">
            {showForm ? (
              <FormContainer onSuccess={() => setShowForm(false)}>
                <SingleFieldForm />
              </FormContainer>
            ): (
              <div><h3>Thank You!</h3></div>
            ) }
          </div>
        </div>
      </div>
      <div className="aspect-ratio-box" />
      <div className={`socials-menu-mobile ${socialMenuActive ? 'active' : ''}`}>
        <div className="inner">
          <button className="btn socials-menu-mobile__close-btn" onClick={handleCloseMenu}>
            <i className="atl-icon atl-icon--close" />
          </button>
          <div className="socials-container">
            {Object.keys(socialIcons).map((icon, index) => (
              <a
                href={socialIcons[icon].href}
                target="_blank"
                className="social-icon"
                {...(socialIcons[icon].track ? {'data-track': socialIcons[icon].track} : {})}
                {...(socialIcons[icon].linkName ? {'custom-link-name': socialIcons[icon].linkName} : {})}
                key={icon}
              >
                <img src={socialIcons[icon].default} className="icon-default" />
                <img src={socialIcons[icon].hover} className="icon-hover" />
              </a>
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default HousePage;
