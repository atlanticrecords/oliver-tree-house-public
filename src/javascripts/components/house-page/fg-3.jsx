import React, { useEffect, useRef } from 'react';
import { fromEvent } from 'rxjs';
import { fg, icons, icons2, linkIcon, linkIconHover, rooms as roomImages } from 'images';
import anime from 'animejs/lib/anime.es.js';
import rooms, { garage } from 'constants/rooms';

const xPad = 90;

function isInViewport(element) {
  const rect = element.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

const Foreground = ({ linkIconHandler, closeMenuHandler, showModal }) => {
  const garageRef = useRef(null);
  const doorRef = useRef(null);
  useEffect(() => {
    const sub = fromEvent(window, 'scroll').subscribe(e => {
      //console.log(e);
      //console.log(isInViewport(garageRef.current));
      if (isInViewport(garageRef.current)) {
        anime({
          targets: doorRef.current,
          y: garage.y + 26 - 280,
          duration: 1000,
        });
      }
    });
    return () => sub.unsubscribe();
  }, []);

  return (
    <svg
      viewBox="0 0 2920 2209"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      className="fg-container"
    >
      <image href={fg} x="0" y="0" />
      {rooms.map((room, index) => (
        <a
          key={room.url}
          xlinkHref={room.href || '#'}
          target="_blank"
          className={`room${room.locked ? ' room--locked' : ''}`}
          {...(room.linkName ? {'custom-link-name': room.linkName} : {})}
          onClick={e => {
            if (room.locked || room.showModal) e.preventDefault();
            if (room.showModal) showModal({ type: 'VideoEmbed' });
          }}
        >
          <image
            href={room.url}
            x={room.x}
            y={room.y + 26}
            width={room.w}
            height={room.h}
          />
          {index !== 0 && <rect
               x={room.x}
               y={room.y + 26}
               width={room.w}
               height={room.h}
          /> }
        </a>
      ))}
      <a
        xlinkHref={'https://www.metatech.industries'}
        target="_blank"
        custom-link-name="Safe Hatch"
        className={''}
      >
        <rect
          x={2300}
          y={2090}
          fill="transparent"
          width={180}
          height={140}
        />
      </a>
      <defs>
        <clipPath id="garageClip">
          <rect
            x={garage.x}
            y={garage.y + 26}
            width={garage.w}
            height={garage.h - 10}
          />
        </clipPath>
      </defs>
      <a
        key={garage.url}
        xlinkHref={garage.href || '#'}
        target="_blank"
        className={`room room--garage ${garage.locked ? ' room--locked' : ''}`}
        custom-link-name="Garage"
        ref={garageRef}
        width={garage.w}
        height={garage.h}
        onClick={e => {
          if (garage.locked || garage.showModal) e.preventDefault();
          if (garage.showModal) showModal({ type: 'VideoEmbed' });
        }}
      >
        <image
          href={garage.url}
          x={garage.x}
          y={garage.y + 26}
          width={garage.w}
          height={garage.h}
        />
        <image
          href={roomImages.garageDoor}
          x={garage.x}
          y={garage.y + 26}
          width={garage.w}
          height={garage.h}
          className="garage-door"
          ref={doorRef}
          style={{ clipPath: 'url(#garageClip)' }}
        />
        <rect
          x={garage.x}
          y={garage.y + 26}
          width={garage.w}
          height={garage.h}
        />
      </a>
      <svg
        x={rooms[4].x}
        y={rooms[4].y + 26}
        width={rooms[4].w}
        height={rooms[4].h}
        className="social-container"
      >
        <rect className="hover-block" width="100%" height="100%" />
        <g className="link-icon" onClick={linkIconHandler} transform="translate(5 5)">
          <image href={linkIcon} className="icon-default" width="84" height="84" />
          <image href={linkIconHover} className="icon-hover" width="84" height="84" />
        </g>
        <g className="socials-group" transform="translate(75 65)">
          {Object.keys(icons).map((icon, index) => (
            <a
              key={index}
              xlinkHref={icons[icon].href}
              target="_blank"
              {...(icons[icon].track ? {'data-track': icons[icon].track} : {})}
              {...(icons[icon].linkName ? {'custom-link-name': icons[icon].linkName} : {})}
              className="social-icon"
            >
              <image
                href={icons[icon].default}
                className="icon-default"
                x={index * xPad}
                y={0}
              />
              <image
                href={icons[icon].hover}
                className="icon-hover"
                x={index * xPad}
                y={0}
              />
            </a>
          ))}
          {Object.keys(icons2).map((icon, index) => (
            <a
              key={index}
              xlinkHref={icons2[icon].href}
              {...(icons2[icon].track ? {'data-track': icons2[icon].track} : {})}
              {...(icons2[icon].linkName ? {'custom-link-name': icons2[icon].linkName} : {})}
              target="_blank"
              className="social-icon"
            >
              <image
                href={icons2[icon].default}
                className="icon-default"
                x={index * xPad}
                y={90}
              />
              <image
                href={icons2[icon].hover}
                className="icon-hover"
                x={index * xPad}
                y={90}
              />
            </a>
          ))}
        </g>
      </svg>
    </svg>
  );
};

export default Foreground;
