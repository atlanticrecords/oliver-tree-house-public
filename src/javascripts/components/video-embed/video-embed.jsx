import React from 'react';

const VideoEmbed = () => {
  return (
    <div>
      <div className="embed-responsive embed-responsive-16by9">
        <iframe
          width="560"
          height="315"
          src="https://www.youtube-nocookie.com/embed/videoseries?list=PLgQwti-5aPoGKrDwAdpAMc-PoNqX1DtTA&enablejsapi=1"
          title="YouTube video player"
          id="video1"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </div>
    </div>
  );
};

export default VideoEmbed;
