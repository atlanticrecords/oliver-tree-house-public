// Load Spotify SDK
if( typeof SpotifyWebApi === 'undefined' )
{
    var SpotifyWebApi=function(){var t="https://api.spotify.com/v1",e=null,r=null,o=function(t,e){return t.abort=e,t},n=function(){var t=Array.prototype.slice.call(arguments),e=t[0],r=t.slice(1);return e=e||{},r.forEach(function(t){for(var r in t)t.hasOwnProperty(r)&&(e[r]=t[r])}),e},a=function(t,n){var a=new XMLHttpRequest,s=function(r,o){var s=t.type||"GET";if(a.open(s,function(t,e){var r="";for(var o in e)if(e.hasOwnProperty(o)){var n=e[o];r+=encodeURIComponent(o)+"="+encodeURIComponent(n)+"&"}return r.length>0&&(t=t+"?"+(r=r.substring(0,r.length-1))),t}(t.url,t.params)),e&&a.setRequestHeader("Authorization","Bearer "+e),t.contentType&&a.setRequestHeader("Content-Type",t.contentType),a.onreadystatechange=function(){if(4===a.readyState){var t=null;try{t=a.responseText?JSON.parse(a.responseText):""}catch(t){console.error(t)}a.status>=200&&a.status<300?function(t){r&&r(t),n&&n(null,t)}(t):(o&&o(a),n&&n(a,null))}},"GET"===s)a.send(null);else{var i=null;t.postData&&(i="image/jpeg"===t.contentType?t.postData:JSON.stringify(t.postData)),a.send(i)}};return n?(s(),null):function(t,e){var n;if(null!==r){var a=r.defer();t(function(t){a.resolve(t)},function(t){a.reject(t)}),n=a.promise}else window.Promise&&(n=new window.Promise(t));return n?new o(n,e):null}(s,function(){a.abort()})},s=function(t,e,r,o){var s={},i=null;return"object"==typeof e?(s=e,i=r):"function"==typeof e&&(i=e),"GET"!==(t.type||"GET")&&t.postData&&!o?t.postData=n(t.postData,s):t.params=n(t.params,s),a(t,i)},i=function(){};return(i.prototype={constructor:SpotifyWebApi}).getGeneric=function(t,e){return s({url:t},e)},i.prototype.getMe=function(e,r){return s({url:t+"/me"},e,r)},i.prototype.getMySavedTracks=function(e,r){return s({url:t+"/me/tracks"},e,r)},i.prototype.addToMySavedTracks=function(e,r,o){return s({url:t+"/me/tracks",type:"PUT",postData:e},r,o)},i.prototype.removeFromMySavedTracks=function(e,r,o){return s({url:t+"/me/tracks",type:"DELETE",postData:e},r,o)},i.prototype.containsMySavedTracks=function(e,r,o){var n={url:t+"/me/tracks/contains",params:{ids:e.join(",")}};return s(n,r,o)},i.prototype.getMySavedAlbums=function(e,r){return s({url:t+"/me/albums"},e,r)},i.prototype.addToMySavedAlbums=function(e,r,o){return s({url:t+"/me/albums",type:"PUT",postData:e},r,o)},i.prototype.removeFromMySavedAlbums=function(e,r,o){return s({url:t+"/me/albums",type:"DELETE",postData:e},r,o)},i.prototype.containsMySavedAlbums=function(e,r,o){var n={url:t+"/me/albums/contains",params:{ids:e.join(",")}};return s(n,r,o)},i.prototype.getMyTopArtists=function(e,r){return s({url:t+"/me/top/artists"},e,r)},i.prototype.getMyTopTracks=function(e,r){return s({url:t+"/me/top/tracks"},e,r)},i.prototype.getMyRecentlyPlayedTracks=function(e,r){return s({url:t+"/me/player/recently-played"},e,r)},i.prototype.followUsers=function(e,r){var o={url:t+"/me/following/",type:"PUT",params:{ids:e.join(","),type:"user"}};return s(o,r)},i.prototype.followArtists=function(e,r){var o={url:t+"/me/following/",type:"PUT",params:{ids:e.join(","),type:"artist"}};return s(o,r)},i.prototype.followPlaylist=function(e,r,o){return s({url:t+"/playlists/"+e+"/followers",type:"PUT",postData:{}},r,o)},i.prototype.unfollowUsers=function(e,r){var o={url:t+"/me/following/",type:"DELETE",params:{ids:e.join(","),type:"user"}};return s(o,r)},i.prototype.unfollowArtists=function(e,r){var o={url:t+"/me/following/",type:"DELETE",params:{ids:e.join(","),type:"artist"}};return s(o,r)},i.prototype.unfollowPlaylist=function(e,r){return s({url:t+"/playlists/"+e+"/followers",type:"DELETE"},r)},i.prototype.isFollowingUsers=function(e,r){var o={url:t+"/me/following/contains",type:"GET",params:{ids:e.join(","),type:"user"}};return s(o,r)},i.prototype.isFollowingArtists=function(e,r){var o={url:t+"/me/following/contains",type:"GET",params:{ids:e.join(","),type:"artist"}};return s(o,r)},i.prototype.areFollowingPlaylist=function(e,r,o){var n={url:t+"/playlists/"+e+"/followers/contains",type:"GET",params:{ids:r.join(",")}};return s(n,o)},i.prototype.getFollowedArtists=function(e,r){return s({url:t+"/me/following",type:"GET",params:{type:"artist"}},e,r)},i.prototype.getUser=function(e,r,o){var n={url:t+"/users/"+encodeURIComponent(e)};return s(n,r,o)},i.prototype.getUserPlaylists=function(e,r,o){var n;return"string"==typeof e?n={url:t+"/users/"+encodeURIComponent(e)+"/playlists"}:(n={url:t+"/me/playlists"},o=r,r=e),s(n,r,o)},i.prototype.getPlaylist=function(e,r,o){return s({url:t+"/playlists/"+e},r,o)},i.prototype.getPlaylistTracks=function(e,r,o){return s({url:t+"/playlists/"+e+"/tracks"},r,o)},i.prototype.createPlaylist=function(e,r,o){var n={url:t+"/users/"+encodeURIComponent(e)+"/playlists",type:"POST",postData:r};return s(n,r,o)},i.prototype.changePlaylistDetails=function(e,r,o){return s({url:t+"/playlists/"+e,type:"PUT",postData:r},r,o)},i.prototype.addTracksToPlaylist=function(e,r,o,n){return s({url:t+"/playlists/"+e+"/tracks",type:"POST",postData:{uris:r}},o,n,!0)},i.prototype.replaceTracksInPlaylist=function(e,r,o){return s({url:t+"/playlists/"+e+"/tracks",type:"PUT",postData:{uris:r}},{},o)},i.prototype.reorderTracksInPlaylist=function(e,r,o,n,a){return s({url:t+"/playlists/"+e+"/tracks",type:"PUT",postData:{range_start:r,insert_before:o}},n,a)},i.prototype.removeTracksFromPlaylist=function(e,r,o){var n=r.map(function(t){return"string"==typeof t?{uri:t}:t});return s({url:t+"/playlists/"+e+"/tracks",type:"DELETE",postData:{tracks:n}},{},o)},i.prototype.removeTracksFromPlaylistWithSnapshotId=function(e,r,o,n){var a=r.map(function(t){return"string"==typeof t?{uri:t}:t});return s({url:t+"/playlists/"+e+"/tracks",type:"DELETE",postData:{tracks:a,snapshot_id:o}},{},n)},i.prototype.removeTracksFromPlaylistInPositions=function(e,r,o,n){return s({url:t+"/playlists/"+e+"/tracks",type:"DELETE",postData:{positions:r,snapshot_id:o}},{},n)},i.prototype.uploadCustomPlaylistCoverImage=function(e,r,o){var n={url:t+"/playlists/"+e+"/images",type:"PUT",postData:r.replace(/^data:image\/jpeg;base64,/,""),contentType:"image/jpeg"};return s(n,{},o)},i.prototype.getAlbum=function(e,r,o){return s({url:t+"/albums/"+e},r,o)},i.prototype.getAlbumTracks=function(e,r,o){return s({url:t+"/albums/"+e+"/tracks"},r,o)},i.prototype.getAlbums=function(e,r,o){var n={url:t+"/albums/",params:{ids:e.join(",")}};return s(n,r,o)},i.prototype.getTrack=function(e,r,o){var n={};return n.url=t+"/tracks/"+e,s(n,r,o)},i.prototype.getTracks=function(e,r,o){var n={url:t+"/tracks/",params:{ids:e.join(",")}};return s(n,r,o)},i.prototype.getArtist=function(e,r,o){return s({url:t+"/artists/"+e},r,o)},i.prototype.getArtists=function(e,r,o){var n={url:t+"/artists/",params:{ids:e.join(",")}};return s(n,r,o)},i.prototype.getArtistAlbums=function(e,r,o){return s({url:t+"/artists/"+e+"/albums"},r,o)},i.prototype.getArtistTopTracks=function(e,r,o,n){return s({url:t+"/artists/"+e+"/top-tracks",params:{country:r}},o,n)},i.prototype.getArtistRelatedArtists=function(e,r,o){return s({url:t+"/artists/"+e+"/related-artists"},r,o)},i.prototype.getFeaturedPlaylists=function(e,r){return s({url:t+"/browse/featured-playlists"},e,r)},i.prototype.getNewReleases=function(e,r){return s({url:t+"/browse/new-releases"},e,r)},i.prototype.getCategories=function(e,r){return s({url:t+"/browse/categories"},e,r)},i.prototype.getCategory=function(e,r,o){return s({url:t+"/browse/categories/"+e},r,o)},i.prototype.getCategoryPlaylists=function(e,r,o){return s({url:t+"/browse/categories/"+e+"/playlists"},r,o)},i.prototype.search=function(e,r,o,n){var a={url:t+"/search/",params:{q:e,type:r.join(",")}};return s(a,o,n)},i.prototype.searchAlbums=function(t,e,r){return this.search(t,["album"],e,r)},i.prototype.searchArtists=function(t,e,r){return this.search(t,["artist"],e,r)},i.prototype.searchTracks=function(t,e,r){return this.search(t,["track"],e,r)},i.prototype.searchPlaylists=function(t,e,r){return this.search(t,["playlist"],e,r)},i.prototype.getAudioFeaturesForTrack=function(e,r){var o={};return o.url=t+"/audio-features/"+e,s(o,{},r)},i.prototype.getAudioFeaturesForTracks=function(e,r){return s({url:t+"/audio-features",params:{ids:e}},{},r)},i.prototype.getAudioAnalysisForTrack=function(e,r){var o={};return o.url=t+"/audio-analysis/"+e,s(o,{},r)},i.prototype.getRecommendations=function(e,r){return s({url:t+"/recommendations"},e,r)},i.prototype.getAvailableGenreSeeds=function(e){return s({url:t+"/recommendations/available-genre-seeds"},{},e)},i.prototype.getMyDevices=function(e){return s({url:t+"/me/player/devices"},{},e)},i.prototype.getMyCurrentPlaybackState=function(e,r){return s({url:t+"/me/player"},e,r)},i.prototype.getMyCurrentPlayingTrack=function(e,r){return s({url:t+"/me/player/currently-playing"},e,r)},i.prototype.transferMyPlayback=function(e,r,o){var n=r||{};return n.device_ids=e,s({type:"PUT",url:t+"/me/player",postData:n},r,o)},i.prototype.play=function(e,r){var o="device_id"in(e=e||{})?{device_id:e.device_id}:null,n={};return["context_uri","uris","offset","position_ms"].forEach(function(t){t in e&&(n[t]=e[t])}),s({type:"PUT",url:t+"/me/player/play",params:o,postData:n},"function"==typeof e?e:{},r)},i.prototype.pause=function(e,r){var o="device_id"in(e=e||{})?{device_id:e.device_id}:null;return s({type:"PUT",url:t+"/me/player/pause",params:o},e,r)},i.prototype.skipToNext=function(e,r){var o="device_id"in(e=e||{})?{device_id:e.device_id}:null;return s({type:"POST",url:t+"/me/player/next",params:o},e,r)},i.prototype.skipToPrevious=function(e,r){var o="device_id"in(e=e||{})?{device_id:e.device_id}:null;return s({type:"POST",url:t+"/me/player/previous",params:o},e,r)},i.prototype.seek=function(e,r,o){var n={position_ms:e};return"device_id"in(r=r||{})&&(n.device_id=r.device_id),s({type:"PUT",url:t+"/me/player/seek",params:n},r,o)},i.prototype.setRepeat=function(e,r,o){var n={state:e};return"device_id"in(r=r||{})&&(n.device_id=r.device_id),s({type:"PUT",url:t+"/me/player/repeat",params:n},r,o)},i.prototype.setVolume=function(e,r,o){var n={volume_percent:e};return"device_id"in(r=r||{})&&(n.device_id=r.device_id),s({type:"PUT",url:t+"/me/player/volume",params:n},r,o)},i.prototype.setShuffle=function(e,r,o){var n={state:e};return"device_id"in(r=r||{})&&(n.device_id=r.device_id),s({type:"PUT",url:t+"/me/player/shuffle",params:n},r,o)},i.prototype.getAccessToken=function(){return e},i.prototype.setAccessToken=function(t){e=t},i.prototype.setPromiseImplementation=function(t){var e=!1;try{var o=new t(function(t){t()});"function"==typeof o.then&&"function"==typeof o.catch&&(e=!0)}catch(t){console.error(t)}if(!e)throw new Error("Unsupported implementation of Promises/A+");r=t},i}();"object"==typeof module&&"object"==typeof module.exports&&(module.exports=SpotifyWebApi);
}

var Mokoala = {
  Utility: {
    openPopup: function( url, window_name, width, height )
    {
      var screen_left = window.screenLeft != undefined ? window.screenLeft : screen.left;
      var screen_top = window.screenTop != undefined ? window.screenTop : screen.top;

      var _width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
      var _height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

      var left = ((_width / 2) - (width / 2)) + screen_left;
      var top = ((_height / 2) - (height / 2)) + screen_top;

      var window_features = "toolbar=no,menubar=no,location=no,resizable=no,scrollbars=yes,status=no,width="+width+",height="+height+",top="+top+",left="+left+"";
      return window.open(url, window_name, window_features);
    },
    buildQuery: function( formdata, numeric_prefix, arg_separator )
    {
      var value, key, tmp = [],
      that = this;
      var _http_build_query_helper = function (key, val, arg_separator)
      {
        var k, tmp = [];
        if (val === true)
        {
          val = "1";
        }
        else if (val === false)
        {
          val = "0";
        }

        if (val != null)
        {
          if(typeof(val) === "object")
          {
            for (k in val)
            {
              if (val[k] != null)
              {
                tmp.push(_http_build_query_helper(key + "[" + k + "]", val[k], arg_separator));
              }
            }

            return tmp.join(arg_separator);
          }
          else if (typeof(val) !== "function")
          {
            return encodeURIComponent(key) + "=" + encodeURIComponent(val);
          }
          else
          {
            throw new Error('There was an error processing your object.');
          }
        }
        else
        {
          return '';
        }
      };

      if (!arg_separator)
      {
        arg_separator = "&";
      }

      for (key in formdata)
      {
        value = formdata[key];
        if (numeric_prefix && !isNaN(key))
        {
          key = String(numeric_prefix) + key;
        }
        var query=_http_build_query_helper(key, value, arg_separator);
        if(query != '')
        {
          tmp.push(query);
        }
      }

      return tmp.join(arg_separator);
    },
        getPlatformFormatted: function( platform ){
            var platform_formatted = '';

            if( platform === 'spotify' )
            {
                platform_formatted = 'Spotify';
            }
            else if( platform === 'vkontakte' )
            {
                platform_formatted = 'VKontakte';
            }
            else if( platform === 'google' )
            {
                platform_formatted = 'YouTube';
            }
            else if( platform === 'apple' )
            {
                platform_formatted = 'Apple Music';
            }
            else if( platform === 'deezer' )
            {
                platform_formatted = 'Deezer';
            }

            return platform_formatted;
        }
  },
    API: {
        request: function( parameters, callback ){
            var base_url = 'https://campaigns.topsify.com/api.php?'+Mokoala.Utility.buildQuery(parameters);

            fetch(base_url, { credentials: 'include' })
                .then(
                    function(response) {
                        if (response.status != 200)
                        {
                            console.log('Looks like there was a problem.', response);
                            return;
                        }

                        response.json().then(function(data){
                            if( callback )
                            {
                                callback(data);
                            }
                        });
                    }
                )
                .catch(function(err) {
                    console.log('Error', err);
                });
        }
    }
};

var WMGConnect = function(identifier, options)
{
  // Is the user returning to this page after signing in?
  self.user_returned = window.location.hash === '#wmgconnect';

    // Base campaign object
    self.campaign = {
        getTotalStreams: function(callback)
        {
            Mokoala.API.request({
                service: 'campaigns',
                action: 'getTotalStreams',
                id: self.campaign_data.id
            }, callback);
        },
        getStreamsUserLeaderboard: function(callback)
        {
            Mokoala.API.request({
                service: 'campaigns',
                action: 'getStreamsUserLeaderboard',
                id: self.campaign_data.id
            }, callback);
        },
        getStreamsCountryLeaderboard: function(callback)
        {
            Mokoala.API.request({
                service: 'campaigns',
                action: 'getStreamsCountryLeaderboard',
                id: self.campaign_data.id
            }, callback);
        },
        getStreamsRegionLeaderboard: function(callback)
        {
            Mokoala.API.request({
                service: 'campaigns',
                action: 'getStreamsRegionLeaderboard',
                id: self.campaign_data.id
            }, callback);
        }
    };

    // Base user object
    self.user = {
        spotify: {
            refreshAccessToken: function(callback)
            {
                if( !self.user_data || !self.user_data.id )
                {
                    throw new Error('User must be authenticated & be a Spotify user in order to use this method.');
                }

                Mokoala.API.request({
                    service: 'campaigns.users.spotify',
                    action: 'refreshAccessToken'
                }, callback);
            },
        },
        disconnect: function(callback)
        {
            if( !self.user_data || !self.user_data.id )
            {
                throw new Error('User must be authenticated in order to use this method.');
            }

            self.user_data = undefined;

            if( self.user.onStateChange && typeof self.user.onStateChange === 'function' )
            {
                self.user.onStateChange();
            }

            Mokoala.API.request({
                service: 'campaigns.users',
                action: 'disconnect'
            }, callback);
        },
        getTotalStreams: function(callback)
        {
            if( !self.user_data || !self.user_data.id )
            {
                throw new Error('User must be authenticated in order to use this method.');
            }

            Mokoala.API.request({
                service: 'campaigns.users',
                action: 'getTotalStreams',
                id: self.user_data.id
            }, callback);
        }
    };

    // DSP API Instances
    self.instances = {
        apple_music: null,
        spotify: null,
        deezer: null,
        youtube: null,
        vkontakte: null,

        getAppleMusic: function()
        {
            if( !self.instances.apple_music )
            {
                MusicKit.configure({
                    developerToken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IlBHMlRDMjlZTDMifQ.eyJpc3MiOiI3V0M3S0ZCMkVOIiwiaWF0IjoxNTkwNjAzMzczLCJleHAiOjE1OTA2MTA1NzN9.4lNKNTLQj7ugLd5lnuYr0AMf4tt4VZbvUJMCRGsDCFIQYZ8flsRDkn-G2bHD6hHoLh77J8znkcJr30K4ZWxRUQ',
                    app: {
                        name: 'Warner Music Group',
                        build: '2'
                    }
                });

                self.instances.apple_music = MusicKit.getInstance();
            }

            return self.instances.apple_music;
        },

        getSpotify: function()
        {
            if( !self.instances.spotify )
            {
                self.instances.spotify = new SpotifyWebApi();
                }

            return self.instances.spotify;
        }
    };

  // Get campaign details
  self.campaign_data = {"id":"24093","slug":"alec-benjamin-two-windows","collect_email_list":"[{\"id\":[\"14074592\"],\"label\":\"<p>Sign up to hear more from Topsify.<\/p>\",\"mandatory\":0,\"data_extension\":{\"14074592\":\"\"},\"trigger\":{\"14074592\":\"\"}},{\"id\":[\"\"],\"label\":\"<p>By submitting my information above I consent to the use of my personal information for the competition in accordance with the <a href=\\\"http:\/\/www.wminewmedia.com\/privacy\/\\\" target=\\\"_blank\\\" rel=\\\"noopener\\\">Privacy Policy<\/a> and acknowledge that I have read and accept the <a href=\\\"https:\/\/wminewmedia.com\/terms-of-use\/\\\">Terms of Use<\/a> and <a target=\\\"_blank\\\" rel=\\\"noopener\\\">Official Rules<\/a><\/p>\",\"mandatory\":1,\"data_extension\":{},\"trigger\":{}}]","collect_email_prompt":"<p>By submitting my information I agree to the <a href=\"https:\/\/campaigns.topsify.com\/app\/24093\/alec-benjamin-two-windows\/terms\" target=\"_blank\">Official Rules<\/a> and acknowledge that I have reviewed and agree to the <a href=\"https:\/\/campaigns.topsify.com\/app\/24093\/alec-benjamin-two-windows\/privacy\" target=\"_blank\">Privacy Policy<\/a> and <a href=\"https:\/\/campaigns.topsify.com\/app\/24093\/alec-benjamin-two-windows\/terms\" target=\"_blank\">Terms of Use<\/a>; and I further agree to receive updates and marketing messages from Topsify.<\/p>","collect_email_button_text":"","campaign_name":"ATL_ALECBENJAMIN_DSPGATE","collect_email_thank_you":"","pre_save_enabled":"0","entry_question":"0","entry_question_text":"","option_enabled":"0","option_text":"","option_list":[],"additional_entries_embed_uri":""};
  // Get artist details

  // Get user details, if they've already signed in

  // Set page config options
  var options = {
    opt_in_dialogue_enabled: options && typeof options.opt_in_dialogue_enabled !== 'undefined' ? options.opt_in_dialogue_enabled : true,
        opt_in_thank_you_enabled: options && typeof options.opt_in_thank_you_enabled !== 'undefined' ? options.opt_in_thank_you_enabled : true,
        exacttarget_datasource: options && typeof options.exacttarget_datasource !== 'undefined' ? options.exacttarget_datasource : '',
  };

  self.multiple_opt_in_enabled = 0;

  // Load Google Analytics
  if( typeof ga === 'undefined' )
  {
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  }

  ga('create', 'UA-21138983-10', 'auto', 'SDK');

  // Load Apple Music SDK
  var script_applemusic = document.createElement('script');
  script_applemusic.src = 'https://js-cdn.music.apple.com/musickit/latest/musickit.js';
  document.head.appendChild(script_applemusic);

  // Load Spotify SDK
  //var script_spotify = document.createElement('script');
  //script_spotify.src = 'https://campaigns.topsify.com/assets/js/spotify.js';
  //document.head.appendChild(script_spotify);

    // Add apple music icon
  var link_applemusic_icon = document.createElement('link');
    link_applemusic_icon.rel = 'apple-music-app-icon';
    link_applemusic_icon.href = 'https://campaigns.topsify.com/assets/img/favicon-wmg.png';
  document.head.appendChild(link_applemusic_icon);

  // append styles for visual elements
  var link_stylesheet = document.createElement('link');
    link_stylesheet.rel = 'stylesheet';
    link_stylesheet.type = 'text/css';
    link_stylesheet.href = 'https://campaigns.topsify.com/app/sdk/platform.css';
  document.head.appendChild(link_stylesheet);

    // Select all created buttons
  self.buttons = identifier ? document.querySelectorAll(identifier) : [];

    self.buttons.forEach(function(button)
    {
        if( !button.getAttribute('data-platform') )
        {
            button.setAttribute('data-platform', 'spotify');
        }
    });

  self.base_url = 'https://campaigns.topsify.com/';
    self.page_url = window.location.href.split('#')[0];
    self.list_ids = [];

    if( self.campaign_data )
    {
        ga('SDK.send', 'pageview', '/app/'+self.campaign_data.id+'/'+self.campaign_data.slug);

        options.opt_in_thank_you_text = self.campaign_data.collect_email_thank_you ? self.campaign_data.collect_email_thank_you : ( self.campaign_data.pre_save_enabled == 1 ? '<p>Thank you for pre-saving!</p>' : '<p>Thank you for entering!</p>' );

        self.button_urls = {
            spotify: self.base_url+'app/connect2/spotify?campaign='+self.campaign_data.id+'&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url),
            deezer: self.base_url+'app/connect2/deezer?campaign='+self.campaign_data.id+'&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url),
            google: self.base_url+'app/connect2/google?campaign='+self.campaign_data.id+'&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url),
            vkontakte: self.base_url+'app/connect2/vkontakte?campaign='+self.campaign_data.id+'&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url),
            apple: self.base_url+'app/connect2/apple?campaign='+self.campaign_data.id+'&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url)
        }

        var button_collect_email_list = self.campaign_data.collect_email_list ? JSON.parse(self.campaign_data.collect_email_list) : {};
        var button_collect_email_list_non_gdpr = self.campaign_data.collect_email_prompt ? self.campaign_data.collect_email_prompt : '';
        var button_collect_email_button_text = self.campaign_data.collect_email_button_text ? self.campaign_data.collect_email_button_text : 'Continue';
    }
    else if( self.artist_data )
    {
        ga('SDK.send', 'pageview', '/app/artist/'+self.artist_data.id+'');

        options.opt_in_thank_you_text = self.artist_data.collect_email_thank_you;

        self.button_urls = {
            spotify: self.base_url+'app/connect2/spotify?artist='+self.artist_data.id+'&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url),
            deezer: self.base_url+'app/connect2/deezer?artist='+self.artist_data.id+'&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url),
            apple: self.base_url+'app/connect2/apple?artist='+self.artist_data.id+'&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url)
        }

        var button_collect_email_list = self.artist_data.collect_email_list ? JSON.parse(self.artist_data.collect_email_list) : {};
        var button_collect_email_list_non_gdpr = self.artist_data.collect_email_prompt ? self.artist_data.collect_email_prompt : '';
        var button_collect_email_button_text = self.artist_data.collect_email_button_text ? self.artist_data.collect_email_button_text : 'Continue';
    }

    button_collect_email_list.forEach(function( item ){
        if( item.id )
        {
            if( Array.isArray(item.id) && item.id.length > 0 )
            {
                self.list_ids = self.list_ids.concat( item.id.filter(word => word.length > 0) );
            }
            else if( String(item.id).trim().length > 0 )
            {
                self.list_ids = self.list_ids.concat( [item.id] );
            }
        }
    });

  self.completeProcess = function(e){
        try
    {
            if( e.data )
            {
                var _request = JSON.parse(e.data);

                if ( _request && _request.user )
                {
                    var modal_container = document.querySelector('.campaigns-modal');

                    if( modal_container)
                    {
                        modal_container.parentNode.removeChild(modal_container);
                    }

                    if( _request.user.spotify_access_token )
                    {
                        var spotify = self.instances.getSpotify();
                        spotify.setAccessToken(_request.user.spotify_access_token);
                    }

                    ga('SDK.send', 'event', 'Follow', 'Followed "'+self.platform_formatted+'"', self.campaign_data ? self.campaign_data.campaign_name : self.artist_data.name);

                    if( options.opt_in_thank_you_enabled && ( options.opt_in_thank_you_text.length > 0 ) )
                    {
                        var modal_container = document.createElement('div');

                        modal_container.innerHTML = '<div class="campaigns-modal-dialogue-container"><div class="campaigns-modal-dialogue campaigns-modal-dialogue-thanks"><button type="button" class="campaigns-modal-dialogue-close">&times;</button>'+options.opt_in_thank_you_text+'</div></div>';

                        modal_container.setAttribute('class', 'campaigns-modal');
                        document.body.appendChild(modal_container);

                        modal_container.querySelector('.campaigns-modal-dialogue-close').onclick = function(){
                            document.body.removeChild(modal_container);
                        };
                    }

                    if( self.callback_completed && typeof self.callback_completed === 'function' )
                    {
                        self.user_data = _request.user;
                        self.callback_completed(_request);
                    }
                }
            }
    }
    catch( e )
    {
            console.log(e);
    }
  };

  if( self.user_returned && self.user_data )
  {
        window.addEventListener('load', function(){
            self.completeProcess({
                data: JSON.stringify({
                    user: self.user_data,
                    service: self.user_data.source
                })
            });
        });
  }

    self.buttons.forEach(function(button){
        button.onclick = function(){
            var platform = button.getAttribute('data-platform');
            var platform_formatted = Mokoala.Utility.getPlatformFormatted(platform);
            var list_ids = [];

            button_collect_email_list.forEach(function( item ){
                if( item.id )
                {
                    if( Array.isArray(item.id) && item.id.length > 0 )
                    {
                        list_ids = list_ids.concat( item.id.filter(word => word.length > 0) );
                    }
                    else if( String(item.id).trim().length > 0 )
                    {
                        list_ids = list_ids.concat( [item.id] );
                    }
                }
            });

            ga('SDK.send', 'event', 'Follow', 'Clicked "'+platform_formatted+'" Button', self.campaign_data ? self.campaign_data.campaign_name : self.artist_data.name);

            if( options.opt_in_dialogue_enabled )
            {
                var modal_container = document.createElement('div');

                var modal_container_inner = '<ul class="opt-in-list">';
                var button_counter = 0;
                button_collect_email_list.forEach(function( item ){
                    button_counter++;

          if( item.label )
          {
                      modal_container_inner+= '<li data-required="'+( item.mandatory )+'"><input id="wmg-sdk-'+button_counter+'" type="checkbox" value="'+( Array.isArray(item.id) ? item.id.join(',') : item.id )+'" /><label for="wmg-sdk-'+button_counter+'">'+item.label+'</label></li>';
          }
                });
                modal_container_inner+= '</ul>';

                // Overwrite multiple checkboxes with a single one for non GDPR
                if( !self.multiple_opt_in_enabled )
                {
                    var modal_container_inner = '<ul class="opt-in-list"><li data-required="1"><input type="checkbox" value="'+list_ids.join(',')+'" />'+button_collect_email_list_non_gdpr+'</li></ul>';
                }

                if( self.campaign_data && self.campaign_data.entry_question == 1 )
                {
                    modal_container_inner+= '<div data-entry_question><label>'+self.campaign_data.entry_question_text+'</label><textarea></textarea></div>';
                }

                if( self.campaign_data && self.campaign_data.option_enabled == 1 )
                {
                    modal_container_inner+= '<div data-entry_option><label>'+self.campaign_data.option_text+'</label>';
                    modal_container_inner+= '<select>';

                    self.campaign_data.option_list.forEach(function(option_list_item){
                        modal_container_inner+= '<option>'+option_list_item+'</option>';
                    });

                    modal_container_inner+= '</select>';
                    modal_container_inner+= '</div>';
                }

                modal_container.innerHTML = '<div class="campaigns-modal-dialogue-container"><div class="campaigns-modal-dialogue"><button type="button" class="campaigns-modal-dialogue-close">&times;</button>'+modal_container_inner+'<div class="opt-in-button"><button>'+button_collect_email_button_text+'</button></div></div></div>';

                modal_container.setAttribute('class', 'campaigns-modal');
                document.body.appendChild(modal_container);

                modal_container.querySelector('.campaigns-modal-dialogue-close').onclick = function(){
                    document.body.removeChild(modal_container);
                };

                modal_container.querySelector('.opt-in-button button').onclick = function(){
                    var error_container = modal_container.querySelector('.error-container');

                    if( error_container )
                    {
                        error_container.parentNode.removeChild(error_container);
                    }

                    var errors = [];
                    var selected_list_ids = [];

                    var entry_question_answer_element = document.querySelector('[data-entry_question] textarea');
                    var entry_option_answer_element = document.querySelector('[data-entry_option] select');

                    var entry_question_answer_value = entry_question_answer_element ? entry_question_answer_element.value : '';
                    var entry_option_answer_value = entry_option_answer_element ? entry_option_answer_element.value : '';

                    if( entry_question_answer_element && !entry_question_answer_value )
                    {
                        errors.push('Please answer the question above.');
                    }

                    if( entry_option_answer_element && !entry_option_answer_value )
                    {
                        errors.push('Please choose an option from the list above.');
                    }

                    document.querySelectorAll('.opt-in-list li').forEach(function(list_item){
                        var required = parseInt( list_item.getAttribute('data-required') );
                        var checked = list_item.querySelector('input').checked;
                        var list_ids = list_item.querySelector('input').value;

                        // Field is not checked but is required
                        if( !checked && required )
                        {
                            errors.push('Please agree to the Terms & Conditions above.');
                        }

                        // Field is checked
                        if( checked && list_ids )
                        {
                            list_ids_array = list_ids.split(',');
                            selected_list_ids = selected_list_ids.concat(list_ids_array);
                        }
                    });

                    if( errors.length > 0 )
                    {
                        var error_container = document.createElement('div');
                            error_container.setAttribute('class', 'error-container error-container-center');

                        errors.forEach(function(error){
                            var error_item = document.createElement('p');
                                error_item.innerHTML = error;

                            error_container.appendChild(error_item);
                        });

                        modal_container_button = modal_container.querySelector('.opt-in-button');

                        modal_container_button.parentNode.insertBefore(error_container, modal_container_button);
                    }
                    else
                    {
                        self.startProcess(platform, selected_list_ids, { entry_question_answer: entry_question_answer_value, entry_option_answer: entry_option_answer_value });
                    }
                };
            }
            else
            {
                self.startProcess(platform, list_ids);
            }
        };
    });

    self.startProcess = function(platform, selected_list_ids, additional_data)
    {
        window.addEventListener('message', self.completeProcess);

        var selected_list_ids = selected_list_ids || [];
        var additional_data = additional_data || {};
        var platform_formatted = Mokoala.Utility.getPlatformFormatted(platform);

        ga('SDK.send', 'event', 'Follow', 'Agreed to Opt-In via "'+platform_formatted+'"', self.campaign_data ? self.campaign_data.campaign_name : self.artist_data.name);

        var selected_list_ids_url_parts = [];
        var selected_option_answer = '';
        var selected_question_answer = '';

        if( selected_list_ids )
        {
            for( var selected_list_ids_key in selected_list_ids )
            {
                if( selected_list_ids.hasOwnProperty(selected_list_ids_key) )
                {
                    var selected_list_ids_value = selected_list_ids[selected_list_ids_key];
                    selected_list_ids_url_parts.push('user_subscribed_to_lists[]='+selected_list_ids_value);
                }
            }
        }

        if( platform === 'apple' )
        {
            var music = self.instances.getAppleMusic();

            music.authorize()
                .then(function(apple_music_user_token){
                    if( self.campaign_data )
                    {
                        var popup_url = self.base_url+'app/connect2/apple?campaign='+self.campaign_data.id+'&user_agreed_to_terms_and_conditions=1&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url)+'&apple_music_access_token='+encodeURIComponent(apple_music_user_token)+( selected_list_ids_url_parts.length ? '&'+selected_list_ids_url_parts.join('&') : '' );
                    }
                    else if( self.artist_data )
                    {
                        var popup_url = self.base_url+'app/connect2/apple?artist='+self.artist_data.id+'&user_agreed_to_terms_and_conditions=1&embed_widget=true&exacttarget_datasource='+options.exacttarget_datasource+'&exacttarget_referrer='+encodeURIComponent(self.page_url)+'&apple_music_access_token='+encodeURIComponent(apple_music_user_token)+( selected_list_ids_url_parts.length ? '&'+selected_list_ids_url_parts.join('&') : '' );
                    }

                    if( !Mokoala.Utility.openPopup( popup_url, 'connect', 500, 600 ) )
                    {
                        location.href = popup_url+'&redirect=true';
                    }
                });
        }
        else
        {
            var popup_url = self.button_urls[platform]
                                +'&user_agreed_to_terms_and_conditions=1'
                                +( selected_list_ids_url_parts.length ? '&'+selected_list_ids_url_parts.join('&') : '' )
                                +( additional_data.entry_question_answer ? '&additional_information='+encodeURIComponent(additional_data.entry_question_answer) : '' )
                                +( additional_data.entry_option_answer ? '&option_answer='+encodeURIComponent(additional_data.entry_option_answer) : '' );

            if( !Mokoala.Utility.openPopup( popup_url, 'connect', 500, 600 ) )
            {
                location.href = popup_url+'&redirect=true';
            }
        }
    };

    return {
        campaign: {
            getData: function()
            {
                return self.campaign_data;
            },
            getTotalStreams: function(callback)
            {
                return self.campaign.getTotalStreams(callback);
            },
            getStreamsUserLeaderboard: function(callback)
            {
                return self.campaign.getStreamsUserLeaderboard(callback);
            },
            getStreamsCountryLeaderboard: function(callback)
            {
                return self.campaign.getStreamsCountryLeaderboard(callback);
            },
            getStreamsRegionLeaderboard: function(callback)
            {
                return self.campaign.getStreamsRegionLeaderboard(callback);
            }
        },
        user: {
            spotify: {
                refreshAccessToken: function(callback)
                {
                    return self.user.spotify.refreshAccessToken(callback);
                },
            },
            disconnect: function(callback)
            {
                return self.user.disconnect(callback);
            },
            isAuthenticated: function()
            {
                return self.user_data && self.user_data.id ? true : false;
            },
            getData: function()
            {
                return self.user_data;
            },
            getTotalStreams: function(callback)
            {
                return self.user.getTotalStreams(callback);
            }
        },
        instances: {
            getAppleMusic: function()
            {
                return self.instances.getAppleMusic();
            },
            getSpotify: function()
            {
                return self.instances.getSpotify();
            }
        },
        setCallback: function( user_callback )
        {
            self.callback_completed = user_callback;
        },
        startProcess: function( service )
        {
            self.startProcess( service, self.list_ids );
        }
    }
}

export default WMGConnect;
