import snakeToCamel from './snake-to-camel';
import createAction from './create-action';

export default actions =>
  Object.fromEntries(
    Object.keys(actions).map(action => [
      snakeToCamel(action.toLowerCase()),
      createAction(actions[action]),
    ])
  );
