import { MLIST_CONFIG } from 'utils/config';
import { trackSignup } from 'utils/tracking';
import jsonp from 'jsonp';
import queryString from 'query-string';

const baseURL = 'https://signup.wmg.com/register?geoip=true';

const handleSuccess = response => {
  if (response.status === 'success') {
    trackSignup('spotify email sign-up');
  }
};

const getConfig = () => {
  let data = {
    newsletterId: MLIST_CONFIG.id,
    Datasource: MLIST_CONFIG.datasource,
  };

  if (MLIST_CONFIG.ext) {
    data._ext = MLIST_CONFIG.ext;
  }

  return data;
};

const sendRequestJsonP = params => {
  return new Promise((resolve, reject) => {
    jsonp(
      `${baseURL}&${queryString.stringify(params)}`,
      {
        param: 'jsonp'
      },
      (err, response) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(response);
      },
    );
  });
};

const scrapeEmail = async options => {
  let newParams = { ...options };
  if (typeof options === 'string') {
    newParams = { email: options };
  }
  const params = { ...getConfig(), ...newParams };
  const response = await sendRequestJsonP(params);
  return response;
};

const contestSignup = async contestParams => {
  const params = { ...getConfig(), ...contestParams };
  const response = await sendRequestJsonP(params);
  return response;
};

export default {};
export { scrapeEmail, contestSignup, getConfig };
