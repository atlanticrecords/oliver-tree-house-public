import React, { useState, useEffect, useRef } from 'react';

function useInterval(callback, delay) {
  const savedCallback = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

function useInput(initialValue, opts = {}) {
  const [value, setValue] = useState(initialValue);

  function onChange(e) {
    const newValue = e.target.value;
    let shouldUpdate = true;
    if (typeof opts.validate === 'function') {
      shouldUpdate = opts.validate(newValue, value);
    }
    if (shouldUpdate) {
      setValue(newValue);
    }
  }

  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  const handler = {
    value,
    setValue,
    onChange,
  };

  return handler;
}

export { useInterval, useInput };
