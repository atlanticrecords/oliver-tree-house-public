const pluralize = (noun, count, suffix = 's') => `${noun}${count !== 1 ? suffix : ''}`;

export { pluralize };
