import camelToSnake from './camel-to-snake';

export default state => Object.fromEntries(Object.keys(state).map(key => [`SET_${camelToSnake(key).toUpperCase()}`, `SET_${camelToSnake(key).toUpperCase()}`]));
