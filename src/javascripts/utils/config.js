export const PAGE_CONFIG = window.atlConfig.page || {};
export const MLIST_CONFIG = window.atlConfig.mlist || {};
export const OMNITURE_CONFIG = window.atlConfig.omniture || {};
export const SOCIALS_CONFIG = window.atlConfig.socials || {};
