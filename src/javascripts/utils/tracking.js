import { OMNITURE_CONFIG } from './config';

/* eslint-disable no-underscore-dangle */
const trackSignup = (str = 'email sign-up') => {
  if (window._satellite) {
    window._satellite.track(str);
  }
};

const trackPage = (pageName, pageType) => {
  if (window.digitalData) {
    const pn = OMNITURE_CONFIG.page.pageInfo.pageName.slice(0, OMNITURE_CONFIG.page.pageInfo.pageName.lastIndexOf(':'));
    const pt = OMNITURE_CONFIG.page.pageInfo.pageType.slice(0, OMNITURE_CONFIG.page.pageInfo.pageType.lastIndexOf(':'));

    window.digitalData.page.pageInfo.pageName = `${pn}:${pageName}`;

    if (pageType) {
      window.digitalData.page.pageInfo.pageType = `${pageType}`;
    }

    // console.log(window.digitalData.page.pageInfo.pageName, window.digitalData.page.pageInfo.pageType);
    window._satellite.track('page view');
  }
};
/* eslint-enable no-underscore-dangle */

const resetPageData = () => {
  if (window.digitalData) {
    window.digitalData.page.pageInfo.pageName = OMNITURE_CONFIG.page.pageInfo.pageName;
    window.digitalData.page.pageInfo.pageType = OMNITURE_CONFIG.page.pageInfo.pageType;
  }
};

const trackClick = (str, page) => {
  if (window.digitalData) {
    // const OMNITURE_CONFIG = window.atlConfig.onmiture || {};
    if (OMNITURE_CONFIG.settings.reportSuites === '') {
      return;
    }

    let o = `${window.digitalData.page.pageInfo.pageName}`;

    if (page) {
      o += `:${page}`;
    }

    o += `:${str}`;

    if (window.s_gi && window.s_dtm) {
      window.s_dtm.linkTrackVars = 'prop1,eVar4';
      window.s_dtm.prop1 = OMNITURE_CONFIG.content.artist;
      window.s_dtm.eVar4 = OMNITURE_CONFIG.content.artist;
      window.s_dtm.tl(this, 'o', o);
    }
  }
};

export { trackClick, trackPage, resetPageData, trackSignup };
