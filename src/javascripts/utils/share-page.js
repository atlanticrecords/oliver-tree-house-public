import { open } from './popup';
import { stringify } from 'query-string';

const sharePageTwitter = options => {
  open(`https://twitter.com/intent/tweet?${stringify(options)}`, 'Twitter', 600, 440);
};

const sharePageFacebook = options => {
  open(`https://www.facebook.com/sharer.php?u=${encodeURIComponent(options.url)}`, 'Facebook', 600, 440);
};

// const sharePageFacebook = options => {
//   return new Promise((resolve, reject) => {
//     try {
//       FB.ui({
//         method: 'share',
//         href: options.url
//       }, res => resolve(res));
//     } catch (e) {
//       reject(e);
//     }
//   });
// };

export {
  sharePageFacebook,
  sharePageTwitter
};
