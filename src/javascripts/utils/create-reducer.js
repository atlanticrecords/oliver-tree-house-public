import createReducerActions from './create-reducer-actions';
import createActionCreators from './create-action-creators';

export default initialState => {
  const reducerActions = createReducerActions(initialState);
  const actions = createActionCreators(reducerActions);

  return {
    actions,
    reducer: (state = initialState, action) => {
      const { type, payload } = action;
      for (let i = 0; i < Object.keys(reducerActions).length; i++) {
        if (type === reducerActions[type]) {
          return { ...state, ...payload };
        }
      }
      return state;
    },
  };
};
