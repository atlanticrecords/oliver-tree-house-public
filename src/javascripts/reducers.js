import { combineReducers } from 'redux';

import modal from 'components/modal/reducer';
import device from 'components/layout/device/reducer';
import topsify from 'components/topsify-auth-btn/reducer';

const rootReducer = combineReducers({
  topsify,
  ui: combineReducers({
    modal,
    device,
  }),
});

export default rootReducer;
