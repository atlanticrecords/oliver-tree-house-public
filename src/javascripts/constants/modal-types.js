import VideoEmbed from 'components/video-embed';

const modalTypes = {
  VideoEmbed: VideoEmbed,
};

export default modalTypes;
