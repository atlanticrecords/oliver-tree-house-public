import { rooms } from 'images';

const garage = {
  url: rooms.garage,
  locked: true,
  x: 1043,
  y: 1738,
  w: 905,
  h: 359,
};

export default [
  {
    url: rooms.attic,
    href: 'http://olivertree.lnk.to/UIBDeluxeAW',
    linkName: 'Attic Linkfire',
    x: 1275,
    y: 590,
    w: 288,
    h: 288,
  },
  {
    url: rooms.theater,
    showModal: true,
    linkName: 'Movie Theater',
    x: 1038,
    y: 909,
    w: 544,
    h: 252,
  },
  {
    url: rooms.studio,
    linkName: 'Studio',
    x: 1599,
    y: 909,
    w: 354,
    h: 252,
    locked: true,
  },
  {
    url: rooms.closet,
    href: 'https://store.olivertreemusic.com/?intcmp=210528/otree/atl/lan/s_hp/img/bdy/ww/turbos-house-closet',
    linkName: 'Closet',
    x: 1038,
    y: 1176,
    w: 374,
    h: 252,
  },
  {
    url: rooms.bedroom,
    x: 1426,
    y: 1176,
    w: 528,
    h: 252,
    locked: true,
  },
  {
    url: rooms.artroom,
    href: 'https://www.olivertreemusic.com/memes?intcmp=210528/otree/atl/lan/lan/img/bdy/ww/turbos-house-art-room',
    linkName: 'Art Room',
    x: 1038,
    y: 1447,
    w: 475,
    h: 251,
  },
  {
    url: rooms.gameroom,
    linkName: 'Game Room',
    x: 1527,
    y: 1447,
    w: 427,
    h: 251,
    locked: true,
  },
];

export { garage };
