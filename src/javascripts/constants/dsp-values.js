const streamDsps = [
  {
    name: 'spotify',
    label: 'Spotify',
    iconClass: 'fab fa-spotify',
    actionLabel: 'Pre-save & Follow',
    actionDescription: 'Connect with Spotify and follow Artist, pre-save \'track\' and sign up for email updates',
  },
  {
    name: 'apple',
    label: 'Apple Music',
    iconClass: 'fab fa-apple',
    actionLabel: 'Pre-save & Follow',
    actionDescription: 'Connect with Apple Music and follow Artist, pre-add \'track\' and sign up for email updates',
  },
  {
    name: 'amazon',
    label: 'Amazon Music',
    iconClass: 'fab fa-amazon',
    actionLabel: 'Stream',
    streamUrl: 'https://www.amazon.com/Kanye-West/e/B000APR990',
  },
  {
    name: 'google',
    label: 'Youtube',
    iconClass: 'fab fa-youtube',
    actionLabel: 'Follow Artist!',
    actionDescription: 'Connect with Youtube and follow Artist!',
  },
  {
    name: 'tidal',
    label: 'Tidal',
    iconClass: 'icon icon--tidal',
    actionLabel: 'Stream',
    streamUrl: 'https://listen.tidal.com/artist/25022',
  },
  {
    name: 'deezer',
    label: 'Deezer',
    iconClass: 'icon icon--deezer',
    actionLabel: 'Save & Follow',
    actionDescription: 'Connect with Deezer and follow Artist, pre-save \'track\' and sign up for email updates',
  },
];

export default streamDsps;
