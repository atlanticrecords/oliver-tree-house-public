const sliderSettings = {
  infinite: true,
  variableWidth: true,
  slidesToScroll: 2,
  centerMode: false,
  className: 'sticker-list',
};

export default {};
export { sliderSettings };
