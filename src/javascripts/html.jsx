import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { BrowserRouter, Route } from 'react-router-dom';

import configureStore from './utils/configureStore';
import App from './components/app';
import { Loader } from './components/layout';

const { store, persistor } = configureStore();

const initReactApp = () => {
    render(
        <Provider store={store}>
            <PersistGate loading={<Loader />} persistor={persistor}>
              <BrowserRouter basename="/treehouse">
                <Route render={props => <App location={props.location} />} />
              </BrowserRouter>
            </PersistGate>
        </Provider>,
      document.getElementById('app')
    );
};

const createRootElement = () => {
  let element = document.createElement('div');

  element.setAttribute('id','app');

  document.body.appendChild(element);
};

export { createRootElement };
export default initReactApp;
