# Atlantic Records Webpack 4 Config

## Getting Started

```
yarn init
yarn add git+ssh://git@wmg_bitbucket:atlanticrecords/atl-dna-base.git
yarn run dna init # Copies files into root dir
nvm use
cp example.env .env
docker-compose up -d
yarn run dna dev
```

## Production

`yarn run dna build`
